data "aws_iam_policy_document" "s3_bucket_lb" {
  statement {
    sid       = "AllowLB"
    effect    = "Allow"
    actions   = ["s3:PutObject"]
    resources = ["${aws_s3_bucket.ALB_Logger[0].arn}/*"]

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${var.account_id}:root"]
    }
  }
}
data "aws_caller_identity" "current" {}

