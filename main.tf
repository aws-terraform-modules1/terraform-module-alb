resource "aws_lb" "odai_ALB" {
  name                       = var.ALB_Name
  load_balancer_type         = "application"
  enable_deletion_protection = false
  subnets                    = var.ALB_Subnets
  tags = {
    Environment = var.ALB_Env
  }
  access_logs {
    bucket  = aws_s3_bucket.ALB_Logger[0].bucket
    prefix  = var.ALB_Bucket_Name
    enabled = var.ALB_S3_Logs
  }
}

resource "aws_s3_bucket" "ALB_Logger" {
  count  = var.ALB_Bucket ? 1 : 0
  bucket = var.ALB_Bucket_Name
  force_destroy = true
  tags = {
    Name        = var.ALB_Bucket_Name
    Environment = var.ALB_Env
  }
}

module "VPC" {
  source            = "git::https://gitlab.com/aws-terraform-modules1/terraform-module-vpc.git"
  cidr_block        = var.vpc_cidr_block
  subnet_cidr_block = "10.0.0.0/24"
  availability_zone = "us-east-1a"
  Name              = var.VPC_Name
}

resource "aws_subnet" "second_subnet" {
  vpc_id            = module.VPC.VPC_ID
  cidr_block        = "10.0.2.0/24"
  availability_zone = "us-east-1b"
}

resource "aws_internet_gateway" "big_door" {
  vpc_id = module.VPC.VPC_ID
  tags = {
    Name = var.Internet_Gateway_Name
  }
}

resource "aws_s3_bucket_policy" "bucket_policy" {
  bucket = aws_s3_bucket.ALB_Logger[0].id
  policy = data.aws_iam_policy_document.s3_bucket_lb.json
}

resource "aws_lb_target_group" "tg" {
  name        = var.target_group
  target_type = "instance"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = module.VPC.VPC_ID
}

resource "aws_lb_target_group_attachment" "tg_to_ec2" {
  count            = length(var.instance_ids)
  target_group_arn = aws_lb_target_group.tg.arn
  target_id        = element(var.instance_ids, count.index)
  port             = 80
}