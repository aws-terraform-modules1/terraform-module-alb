output "VPC_ID" {
  value = module.VPC.VPC_ID
}

output "subnet_id_1" {
  value = module.VPC.odai_subnet_id
}

output "subnet_id_2" {
  value = aws_subnet.second_subnet.id
}

output "ALB_ID" {
  value = aws_lb.odai_ALB.id
}