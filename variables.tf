variable "ALB_Name" {
  type        = string
  description = "(string, Required) Whatever you want to call your AWS Application LoadBalancer, only alphanumeric"
  default     = "balancerofloadsApplication"
}

variable "ALB_Subnets" {
  type        = set(string)
  description = "(set(string), Optional) The ID of the subnet your instances will be in. (aws_subnet.example.id)"
}

variable "ALB_Env" {
  type        = string
  description = "(string, Optional) Either Production or Development, whatever stage you're in."
}

variable "ALB_Bucket_Name" {
  type        = string
  description = "(string, Optional) if you want to store logs in a bucket for Application LB, this would be its name."
}

variable "ALB_S3_Logs" {
  type        = bool
  description = "(Boolean, Optional) default is false even if you specify a name for the bucket."
  default     = true
}

variable "ALB_Bucket" {
 type = bool
  description = "(Boolean, Optional) specifies if you want this resource to be created or not"
}

variable "vpc_cidr_block" {
  type        = string
  description = "(string, optional) The IPv4 CIDR block for the VPC. CIDR can be explicitly set or it can be derived from IPAM using ipv4_netmask_length"
  default     = "10.0.0.0/16"
}

variable "VPC_Name" {
  type        = string
  description = "(string, required) Name of VPC on AWS"
  default     = "Useless Network"
}

variable "Internet_Gateway_Name" {
  type        = string
  description = "(string, Required) Whatever you want to call your internet gateway on AWS"
}

variable "account_id" {
  type = string
  description = "(string, Required) AWS Account ID"
  
}

variable "target_group" {
  type =  string
  description = "(string, Required) whatever you want to call the Target group"
}

variable "instance_ids" {
  type    = list(string)
  description = "(list(string), Required) list containing ec2 instance IDs that you want to be managed by the Application Load Balancer"
}